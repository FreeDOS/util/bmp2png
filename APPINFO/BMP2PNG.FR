Begin3
Language:    FR, 850, French (Standard)
Title:       bmp2png
Description: Convertir les images BMP en PNG
Summary:     Outil en ligne de commande qui convertit les images BMP en images PNG (et vice versa)
Keywords:    bmp,png
End
